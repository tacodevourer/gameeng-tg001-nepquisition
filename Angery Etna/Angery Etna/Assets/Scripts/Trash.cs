﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Trash : MonoBehaviour
{
    public LayerMask LayerMask;
    public string NextScene;

    void OnTriggerEnter2D(Collider2D collision)
    {
        int collisionLayerMask = 1 << collision.gameObject.layer;

        // If collides with player
        if (collisionLayerMask == LayerMask.value)
        {
            SceneManager.LoadScene(NextScene); // Hard coded
            Debug.Log("Nep");
        }
    }
}
