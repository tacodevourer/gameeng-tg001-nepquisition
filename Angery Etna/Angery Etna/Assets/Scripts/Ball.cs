﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float maxDragDistance = 2.5f;

    private float releasedDelay;
    private bool isPressed;
    private bool isReleased;
    private Vector2 direction;

    private Rigidbody2D slingRb;
    private Rigidbody2D rb;
    private SpringJoint2D springJoint;
    private LineRenderer lineRenderer;
    private TrailRenderer trailRenderer;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        springJoint = GetComponent<SpringJoint2D>();
        slingRb = springJoint.connectedBody;
        lineRenderer = GetComponent<LineRenderer>();
        trailRenderer = GetComponent<TrailRenderer>();

        releasedDelay = 1 / (springJoint.frequency * 4); // 1/4 to release from center of the slingshot
    }

    void Update()
    {
        if (isPressed)
        {
            DraggingBall();
        }
    }

    private void DraggingBall()
    {
        SetLineRendererPosition();
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); // get mouse position
        float distance = Vector2.Distance(mousePosition, slingRb.position);

        // Prevents ball from being dragged when released
        if (isReleased == false)
        {
            // Clamp drag distance
            if (distance > maxDragDistance)
            {
                direction = (mousePosition - slingRb.position).normalized;
                rb.position = slingRb.position + direction * maxDragDistance;
            }

            else
            {
                rb.position = mousePosition; // Set ball position to mouse position
            }
        }
    }

    private void OnMouseDown()
    {
        isPressed = true;
        rb.isKinematic = true; // prevents ball from dangling while being dragged

        // Prevents line from being drawn while ball is released
        if (isReleased == false)
        {
            lineRenderer.enabled = true; // Enable line renderer
        }
    }

    private void OnMouseUp()
    {
        isPressed = false;
        rb.isKinematic = false;
        lineRenderer.enabled = false; // Hide line renderer
        trailRenderer.enabled = true; // Display trail renderer
        StartCoroutine(ReleaseBall()); // Release the ball
    }

    private IEnumerator ReleaseBall()
    {
        isReleased = true;
        // Waits till the ball reaches 1/4 of the slingshot and then releases the ball from the sling shot
        yield return new WaitForSeconds(releasedDelay);
        springJoint.enabled = false; // Releases the ball from the slingshot
    }

    private void SetLineRendererPosition()
    {
        Vector3[] positions = new Vector3[2]; // Make 2 positions
        positions[0] = rb.position; // 1st Ball position
        positions[1] = slingRb.position; // 2nd Sling shot position
        lineRenderer.SetPositions(positions); // Draw line between them
    }
}
