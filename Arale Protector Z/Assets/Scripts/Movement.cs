﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float xSpeed;
    public float ySpeed;

    private Rigidbody2D rb;
    private Animator animator;

    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
       // animator = this.GetComponent<Animator>();
    }

    public void Move()
    {
       // animator.SetFloat("Speed", Mathf.Abs(rb.velocity.y));

        // Moves the entity
        rb.velocity = new Vector3(xSpeed, ySpeed);
    }
}
