﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerometer : MonoBehaviour
{
    public float Speed;

    private Rigidbody2D rb;
    private float yDirection;

    // Use this for initialization
    void Start ()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }
	
    public void Move()
    {
        yDirection = Input.acceleration.y * Speed * Time.deltaTime;
        rb.velocity = new Vector2(0, yDirection);
    }
}
