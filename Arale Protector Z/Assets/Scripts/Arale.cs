﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arale : MonoBehaviour
{
    Health health;

    private void Start()
    {
        health = GetComponent<Health>();
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        Health health = gameObject.GetComponent<Health>();
        Damage damage = collider.gameObject.GetComponent<Damage>();

        // If Arale collides with an enemy
        if (collider.gameObject.layer == 9)
        {
            health.TakeDamage(damage.DamageVal);
            collider.gameObject.SetActive(false);
        }
    }
}

