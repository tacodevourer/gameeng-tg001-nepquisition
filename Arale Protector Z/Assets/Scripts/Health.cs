﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float MaxHp;
    public float CurrentHP;
    public Image HealthBar;
    
    void Start()
    {
        CurrentHP = MaxHp;
    }

    void Update()
    {
        // Arale has a HP Bar
        if (gameObject.layer == 11)
        {
            if (CurrentHP != HealthBar.fillAmount)
            {
                HealthBar.fillAmount = CurrentHP / MaxHp;
            }
        }
    }

    public void TakeDamage(float damage)
    {
        CurrentHP -= damage;

        // If entity is dead, remove
        if (CurrentHP <= 0)
        {
            // If Arale's health is 0
            if (gameObject.layer == 11)
            {
                SceneHandler.LoadGameOverScreen();
            }
            gameObject.SetActive(false);
        }
    }

    public void TakeHealing(float healValue)
    {
        CurrentHP += healValue;

        if (CurrentHP >= MaxHp)
        {
            CurrentHP = MaxHp;
        }
    }
}
