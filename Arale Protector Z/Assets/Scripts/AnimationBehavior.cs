﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBehavior : MonoBehaviour
{

    public enum AnimationState
    {
        Idle = 0,
        Shoot = 1,
        Special = 2,
    }

    public AnimationState CurrentState;
    private Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        int StateNum = (int)CurrentState;
        animator.SetInteger("CurrentState", StateNum);
    }
}