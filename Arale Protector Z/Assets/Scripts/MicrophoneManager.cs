﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// References:
//  https://www.reddit.com/r/Unity3D/comments/49wuld/best_way_to_implement_microphone_input/
//  http://www.kaappine.fi/tutorials/using-microphone-input-in-unity3d/

public class MicrophoneManager : MonoBehaviour
{
    public float ShootSensitivity;
    public float SpecialMoveSensitivity;

    [HideInInspector]
    public float CurrentMicrophoneLevel;

    private AudioClip microphoneInput;
    private bool microphoneInitialized;

    void Awake()
    {
        //init microphone input
        if (Microphone.devices.Length > 0)
        {
            microphoneInput = Microphone.Start(null, true, 10, 44100);
            microphoneInitialized = true;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        //get mic volume
        int decibel = 128;
        float[] waveData = new float[decibel];
        int micPosition = Microphone.GetPosition(null) - (decibel + 1); // null means the first microphone
        microphoneInput.GetData(waveData, micPosition);

        // Getting a peak on the last 128 samples
        float levelMax = 0;

        for (int i = 0; i < decibel; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        CurrentMicrophoneLevel = Mathf.Sqrt(Mathf.Sqrt(levelMax));
    }
}

