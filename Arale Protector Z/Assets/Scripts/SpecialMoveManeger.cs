﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialMoveManeger : MonoBehaviour
{
    public static float MaxGauge = 100;
    public static float CurrentGauge = 0;   
    public Image SpecialGauge;
    
    private Animator animator;

    void Start()
    {
        animator = this.GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        if (CurrentGauge != SpecialGauge.fillAmount)
        {
            SpecialGauge.fillAmount = CurrentGauge / MaxGauge;
        }
    }

    public void ActivateSpecialMove()
    {
        if (CurrentGauge == MaxGauge)
        {
            animator.SetTrigger("SpecialMove");
            CurrentGauge = 0;

            // Searches for all objects wit the tag Enemy in current scene
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject target in gameObjects)
            {
                target.SetActive(false); // Sets all enemies in scene to false
            }
        }
    }

    public static void IncreaseSpecialGauge(float specialGaugeValue)
    {
        CurrentGauge += specialGaugeValue;

        if (CurrentGauge >= MaxGauge)
        {
            CurrentGauge = MaxGauge;
        }
    }
}
