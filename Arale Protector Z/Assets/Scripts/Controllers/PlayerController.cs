﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    RangeType rangeType;
    Accelerometer accelerometer;
    SpecialMoveManeger specialMoveManager;
    MicrophoneManager microphoneManger;
    AnimationBehavior animationBehavior;

    // Use this for initialization
    void Start()
    {       
        accelerometer = GetComponent<Accelerometer>();
        rangeType = GetComponent<RangeType>();
        specialMoveManager = GetComponent<SpecialMoveManeger>();
        microphoneManger = GetComponent<MicrophoneManager>();
        animationBehavior = GetComponent<AnimationBehavior>();
    }

    // Update is called once per frame
    void Update()
    {
        SpecialMove();
        Fire();
        Move();
    }

    void Move()
    {
        accelerometer.Move();
    }

    void Fire()
    {
        /*
        if (Input.GetMouseButtonDown(0))
        {
            rangeType.Fire(true, 0);
        }
        */
        if (microphoneManger.CurrentMicrophoneLevel > microphoneManger.ShootSensitivity)
        {
            animationBehavior.CurrentState = AnimationBehavior.AnimationState.Shoot;
            rangeType.Fire(true, 0);
        }      
    }

    void SpecialMove()
    { 
        if (microphoneManger.CurrentMicrophoneLevel > microphoneManger.SpecialMoveSensitivity)
        {
            animationBehavior.CurrentState = AnimationBehavior.AnimationState.Special;
            specialMoveManager.ActivateSpecialMove();
        }
    }
}

