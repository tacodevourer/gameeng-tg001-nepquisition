﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public string NextScene;

    void Start()
    {

    }

    /*
    public void LoadScene()
    { 
		SceneManager.LoadScene(NextScene);
        SceneManager.LoadSceneAsync("PersistentData", LoadSceneMode.Additive);
    }
    */
    public void Retry()
    {
        SpecialMoveManeger.CurrentGauge = 0;
        ScoreManager.Score = 0;
        SceneManager.LoadScene("Level_01");
    }

    
    public static void LoadGameOverScreen()
    {
        SceneManager.LoadScene("GameOver");
    }
}
