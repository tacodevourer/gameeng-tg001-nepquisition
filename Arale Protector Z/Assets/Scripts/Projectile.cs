﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class OnDeath : UnityEvent<Projectile> { }

public class Projectile : MonoBehaviour
{
    public OnDeath OnDeath;
    public float ProjectileDamage = 1;
    public float Speed = 8;
    public float Duration = 2.5f;
    public GameObject HitEffect;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Deactivate(Duration));
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //Gets the health
        Health health = collision.gameObject.GetComponent<Health>();
       // Instantiate(HitEffect, transform.position, Quaternion.identity); // Creates a special effect on hit

        // If the projectile hits anything with health component
        if (health)
        {
            ScoreManager.EarnScore(10);
            SpecialMoveManeger.IncreaseSpecialGauge(10);
            health.TakeDamage(ProjectileDamage);
            OnDeath.Invoke(this); 
        }

        // If the projectile hits anything else other than a game object with health component, destroy the projectile
		else OnDeath.Invoke(this);
    }

    private IEnumerator Deactivate(float duration)
    {
        yield return new WaitForSeconds(duration);
        OnDeath.Invoke(this);
    }
}