﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public KeyCode Key1;
    public KeyCode Key2;
    public float Speed;
    public float TripDuration;
    public float RotateSpeed;
    public List<Sprite> spriteList;
    private int counter;

    private bool keyAlternate = false;
    private bool isTripped = false;
    private SpriteRenderer spriteRenderer;
    private Sprite currentSprite;

    private void Start()
    {
        spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        counter = 0;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(Key1) && Input.GetKeyDown(Key2) && isTripped == false)
        {
            Debug.Log("Tripped");
            isTripped = true;
            transform.Rotate(new Vector3(0, 0, -90), RotateSpeed);
            StartCoroutine(Trip());
        }

        else if (Input.GetKeyDown(Key1) && keyAlternate == false)
        {
            transform.position += transform.right * Speed * Time.deltaTime;
            UpdateAnimation();
            keyAlternate = true;
        }
        else if (Input.GetKeyDown(Key2) && keyAlternate == true)
        {
            transform.position += transform.right * Speed * Time.deltaTime;
            UpdateAnimation();
            keyAlternate = false;
        }

    }

    void UpdateAnimation()
    {
        if(isTripped == false)
        {
            spriteRenderer.sprite = spriteList[counter];
            counter++;

            // Loops back to start of sprite list
            if (counter >= spriteList.Count)
            {
                counter = 0;
            }
        }
    }

    IEnumerator Trip()
    {
        Speed = 0;
        yield return new WaitForSeconds(TripDuration);
        transform.Rotate(new Vector3(0, 0, 90), RotateSpeed);
        Speed = 4.0f;
        isTripped = false;
    }
}
