﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boundary : MonoBehaviour {

    public string NextScene;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("yes");
        SceneManager.LoadScene(NextScene);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Debug.Log("yes");
    //    SceneManager.LoadScene(NextScene);
    //}

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("yes");
        SceneManager.LoadScene(NextScene);
    }
}
