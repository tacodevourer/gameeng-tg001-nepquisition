﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float MaxHp;
    public float CurrentHP;
    
    void Start()
    {
        CurrentHP = MaxHp;
    }

    public void TakeDamage(float damage)
    {
        CurrentHP -= damage;

        // If entity is dead, remove
        if (CurrentHP <= 0)
        {
            gameObject.SetActive(false);
            Invoke("LoadScreen", 1f);
        }
    }

    public void TakeHealing(float healValue)
    {
        CurrentHP += healValue;

        if (CurrentHP >= MaxHp)
        {
            CurrentHP = MaxHp;
        }
    }

    public void LoadScreen()
    {
       SceneManager.LoadScene("EndScreen");
    }
}
