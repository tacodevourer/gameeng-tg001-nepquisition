﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDeath : MonoBehaviour {

    void Start()
    {
        Projectile projectile = GetComponent<Projectile>();
        projectile.OnDeath.AddListener(OnDeath);
    }

    public void OnDeath(Projectile projectile)
    {
        gameObject.SetActive(false);  
        // Destroy(gameObject);
    }
}
