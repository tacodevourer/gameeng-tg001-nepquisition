﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeType : MonoBehaviour
{
    public float RateOfFire = 0.4f;
    public float BonusDamage = 0;
    public Transform ProjectileSpawn;
    public GameObject Projectile;
    public GameObject obj;

    public List<GameObject> spawned = new List<GameObject>();
    public List<GameObject> pooledObjects = new List<GameObject>();
    private float elapsedTime;
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        elapsedTime = RateOfFire; // No cooldown at start
    }

    void Update()
    {
        // Update Time
        elapsedTime += Time.deltaTime;
    }

    public void Fire(bool facingRight)
    {
        float projectileRotation;

        if (facingRight)
        {
            projectileRotation = 0;
            OnFire(projectileRotation);
        }

        else
        {
            projectileRotation = -180;
            OnFire(projectileRotation);
        }
    }

    void OnFire(float projectileRotation)
    {
        // Checks if cooldown is ok, then fire
        if (RateOfFire <= elapsedTime)
        {
            animator.SetTrigger("Shoot");
            elapsedTime = 0.0f; // Resets the cooldown
        
            if (pooledObjects.Count > 0)
            {
                obj = pooledObjects[0];
                pooledObjects.RemoveAt(0);
                obj.transform.position = ProjectileSpawn.position;
                obj.transform.rotation = Quaternion.Euler(0, 0, projectileRotation);
                obj.SetActive(true);
            }
            else obj = Instantiate(Projectile, ProjectileSpawn.position, Quaternion.Euler(0, 0, projectileRotation)); // Creates a projectile, spawning off of gun's location

            obj.GetComponent<Rigidbody2D>().velocity = obj.transform.right * Projectile.GetComponent<Projectile>().Speed;  // Creates velocity by getting the Speed from Projectile script
            obj.GetComponent<Projectile>().ProjectileDamage += BonusDamage; // Add Damage

            // Remove spawn OnDeath
            Projectile projectile = obj.GetComponent<Projectile>();
            projectile.OnDeath.AddListener(OnDeath);
        }      
    }

    void OnDeath(Projectile projectile)
    {
        // Pool the object instead of destroying it
        spawned.Remove(obj);
        projectile.gameObject.SetActive(false);
        pooledObjects.Add(projectile.gameObject);
        projectile.OnDeath.RemoveListener(OnDeath);
    }
}

