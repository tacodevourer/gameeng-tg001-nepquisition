﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    RangeType rangeType;
    Direction direction;

    // Use this for initialization
    void Start()
    {     
        rangeType = GetComponent<RangeType>();
        direction = GetComponent<Direction>();
    }

    void Fire()
    {
        rangeType.Fire(direction.GetComponent<Direction>().FacingRight);
    }
}


