﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    private Vector3 min;
    private Vector3 max;
    private int multiplier;
    private float elapsedTime;
    public float CoolDown = 0.3f;

    private void Update()
    {
        // Update Time
        elapsedTime += Time.deltaTime;
        Teleport();
    }

    void GetBounds()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        Vector3[] v = new Vector3[4];
        rectTransform.GetWorldCorners(v);

        // Subtract rectTransform.position so that objects are not offset
        min = v[0] - (Vector3)rectTransform.position;
        max = v[2] - (Vector3)rectTransform.position;
    }

    private void Teleport()
    {
        if (CoolDown <= elapsedTime)
        {
            elapsedTime = 0.0f; // Resets the cooldown

            GetBounds();
            RectTransform rectTransform = GetComponent<RectTransform>();

            float x = Random.Range(min.x, max.x);
            float y = Random.Range(min.y, max.y);
            multiplier = Random.Range(0, 20);
            Vector2 position = new Vector2(x, y);

            rectTransform.anchoredPosition = position * multiplier;
        }
    }
}
