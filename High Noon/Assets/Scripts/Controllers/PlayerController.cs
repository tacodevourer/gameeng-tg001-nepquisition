﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    RangeType rangeType;
    Direction direction;
    Health health;

    // Use this for initialization
    void Start()
    {
        rangeType = GetComponent<RangeType>();
        direction = GetComponent<Direction>();
        health = GetComponent<Health>();
    }

    public void Fire()
    {       
        rangeType.Fire(direction.GetComponent<Direction>().FacingRight);
    }

}

