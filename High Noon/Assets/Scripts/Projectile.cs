﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class OnDeath : UnityEvent<Projectile> { }

public class Projectile : MonoBehaviour
{
    public OnDeath OnDeath;
    public float ProjectileDamage = 1;
    public float Speed = 8;
    public float Duration = 2.5f;
    public GameObject HitEffect;

    //public Transform Shooter;

    // Use this for initialization
    void Start()
    {
        //  Destroy(gameObject, Duration); // destroys projectile after its duration
        StartCoroutine(Deactivate(Duration));
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //Gets the health
        Health health = collision.gameObject.GetComponent<Health>();
        Instantiate(HitEffect, transform.position, Quaternion.identity);

        // If the projectile hits anything with health component
        if (health)
        {
            health.TakeDamage(ProjectileDamage);
        }

        OnDeath.Invoke(this);
    }

    private IEnumerator Deactivate(float duration)
    {
        yield return new WaitForSeconds(duration);
        OnDeath.Invoke(this);
    }
}