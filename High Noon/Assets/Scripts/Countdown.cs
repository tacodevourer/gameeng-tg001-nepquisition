﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Countdown : MonoBehaviour
{
    public float CountdownValue;
    public Text CountdownText;
    public Button FireButton;
    public GameObject JimmyBarnes;
    public UnityEvent OnCountdownEnd;
    public UnityEvent OnCountdownHalf;

    private float countdownHalfValue;

    void Start()
    {
        if (OnCountdownEnd == null) OnCountdownEnd = new UnityEvent();
        if (OnCountdownHalf == null) OnCountdownHalf = new UnityEvent();

        countdownHalfValue = CountdownValue / 2;
    }

    void Update()
    {
        if (CountdownValue <= countdownHalfValue)
        {
            CountdownHalf();
            FireButton.gameObject.SetActive(true); // Displays the fire button
            JimmyBarnes.SetActive(true);
        }

        if (CountdownValue <= 0)
        {
            CountdownEnd();
            FireButton.gameObject.SetActive(false);
        }

        CountdownValue -= Time.deltaTime;
        CountdownValue = Mathf.Clamp(CountdownValue, 0.0f, Mathf.Infinity);

        CountdownText.text = string.Format("{0:00.00}", CountdownValue);
    }

    void CountdownEnd()
    {
        if (OnCountdownEnd != null) OnCountdownEnd.Invoke(); // Broadcast to listeners that countdown has ended
    }

    void CountdownHalf()
    {
        if (OnCountdownHalf != null) OnCountdownHalf.Invoke(); // Broadcast to listeners that countdown is halfway
    }

}
