﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MouseClick : MonoBehaviour
{
    public string NextScene;
    void Update()
    {
        OnMouseClick();
    }

    void OnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                // If mouse clicks a boy
                if (hit.collider.gameObject.layer == 8) SceneManager.LoadScene(NextScene);

                // If mouse clicks a girl
                else if (hit.collider.gameObject.layer == 9) SceneManager.LoadScene("GameOver");
            }
        }
    }
}
